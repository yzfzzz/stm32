#include "uart.h"


 /**
   * 函数名：UART1_Init()
   * 功能： 串口1的初始化
   * 入口参数：波特率
   * 引脚： PA9(TX) | PA10(RX) 
   */
void UART1_Init(int bound)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);

    /*--------------配置串口1GPIO结构体-----------------*/
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin=GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    /*--------------配置串口1USART结构体-----------------*/
    USART_InitStructure.USART_BaudRate=bound;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    USART_Init(USART1,&USART_InitStructure);

    /*--------------配置串口1中断结构体-----------------*/
    NVIC_InitStructure.NVIC_IRQChannel=USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART1,ENABLE); //串口使能
}


 /**
   * 函数名：UART1_Handler()
   * 功能： 串口1的中断服务函数
   * 入口参数：无
   * 引脚： 无 
   */
void USART1_IRQHandler()
{
    uint8_t receivedData;
    if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET)
    {
        receivedData=USART_ReceiveData(USART1);
        USART_SendData(USART1,receivedData);
    }
}





 /**
   * 函数名：UART2_Init()
   * 功能： 串口2的初始化
   * 入口参数：波特率
   * 引脚： PA2(TX) | PA3(RX) 
   */
void UART2_Init(int bound)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
    
    /*--------------配置串口2GPIO结构体-----------------*/
    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin=GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    /*--------------配置串口2USART结构体-----------------*/
    USART_InitStructure.USART_BaudRate=bound;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
    USART_Init(USART2,&USART_InitStructure);

    /*--------------配置串口2中断结构体-----------------*/
    NVIC_InitStructure.NVIC_IRQChannel=USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=2;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART2,ENABLE); //串口使能
}


 /**
   * 函数名：UART2_Handler()
   * 功能： 串口2的中断服务函数
   * 入口参数：无
   * 引脚： 无 
   */
void USART2_IRQHandler()
{
    uint8_t receivedData;
    if (USART_GetITStatus(USART2,USART_IT_RXNE) !=RESET)
    {
        receivedData=USART_ReceiveData(USART2);
        USART_SendData(USART2,receivedData);
    }
}


 /**
   * 函数名：UART3_Init()
   * 功能： 串口3的初始化
   * 入口参数：波特率
   * 引脚： PB10(TX) | PB11(RX) 
   */
void UART3_Init(int bound)
{
    /*
   * 功能： 串口3的GPIO初始化
   */
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);

    GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Pin=GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB,&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOB,&GPIO_InitStructure);
    
    /*
    * 功能  ：串口结构体初始化
    */
    USART_InitStructure.USART_BaudRate=bound;
    USART_InitStructure.USART_StopBits=USART_StopBits_1;
    USART_InitStructure.USART_WordLength=USART_WordLength_8b;
    USART_InitStructure.USART_Parity=USART_Parity_No;
    USART_InitStructure.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;
    USART_InitStructure.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
     USART_Init(USART3,&USART_InitStructure);

     /*--------------配置串口3中断结构体-----------------*/
    NVIC_InitStructure.NVIC_IRQChannel=USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=3;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    USART_ITConfig(USART3,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART3,ENABLE); //串口使能
}

/**
   * 函数名：UART3_Handler()
   * 功能： 串口3的中断服务函数
   * 入口参数：无
   * 引脚： 无 
   */
void USART3_IRQHandler()
{
    uint8_t receivedData;
    if(USART_GetITStatus(USART3,USART_IT_RXNE) != RESET)
    {
        receivedData=USART_ReceiveData(USART3);
        USART_SendData(USART3,receivedData);
    }
}


/*
* 函数名：UART_SendByte(USART_TypeDef *p_USARTx, uint8_t ch)
* 功能：发送一个字节
* 入口参数：USARTx,字节
*/
void UART_SendByte(USART_TypeDef *p_USARTx, uint8_t ch)
{
    /*发送一个字节的数据到USART*/
    USART_SendData(p_USARTx,ch);

    /*等待发送数据的寄存器为空*/
    while(USART_GetFlagStatus(p_USARTx,USART_FLAG_TXE) == RESET);
}



/*
* 函数名：UART_SendString(USART_TypeDef *p_USARTx,char *str)
* 功能：发送字符串
* 入口参数：USARTx,字符串
*/
void UART_SendString(USART_TypeDef *p_USARTx,char *str)
{
    unsigned int k=0;
    do
    {
        UART_SendByte(p_USARTx,*(str+k));
        k++;
    } 
    while(*(str+k) != '\0');
    /*等待发送完成*/
    while (USART_GetFlagStatus(p_USARTx,USART_FLAG_TC)== RESET)
    {}
}


/*
* 函数名：fputc(int ch, FILE *f)
* 功能：重定向c库函数printf到串口，重定向可使用printf
*/
int fputc(int ch, FILE *p)
{
    USART_SendData(USART1,(int)ch);
    while(USART_GetFlagStatus(USART1,USART_FLAG_TXE) == RESET);
    return ch;
}


