#ifndef __HSR04__
#define __HSR04__

#include "sys.h"
#include "usart.h"

#define  PSC             5000
#define  ARR             7200

#define  Trig            GPIO_Pin_5
#define  Trig_Port       GPIOB

#define  Echo            GPIO_Pin_4
#define  Echo_Port       GPIOB

void HSR04_Init(void);
void HSR04_Trig_Open(void);
extern int Distance;

#endif
