#include "led.h"
#include "hsr04.h"
#include "sys.h"
#include "usart.h"
#include "delay.h"

int main() 
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	delay_init();
	LED_Init();
	uart_init(115200);
	HSR04_Init();
	while(1)
	{
		HSR04_Trig_Open();
		delay_ms(500);
		if(Distance>10)
		{
			LED1_Control(1500,0,1);
		}
		else
		{
			LED1_Control(0,0,0);
		}
		
	}
}
