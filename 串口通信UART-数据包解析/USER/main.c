#include "sys.h"
#include "delay.h"
#include "led.h"
#include "uart.h"

int main(void)
{		
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	LED_Init();
	UART1_Init(115200);
	UART2_Init(115200);
	UART3_Init(115200);
	USART_SendData(USART1,1);
	USART_SendData(USART2,2);
	USART_SendData(USART3,3);
	while(1)
	{
		if(USART1_RX_STA&0xC000)
		{
			u8 i;
			u16 USART_SendLength;
			USART_SendLength=USART1_RX_STA&0x3FFF;
			
			if(USART_SendLength==2&&USART1_BUFF[0]=='o'&&USART1_BUFF[1]=='n')
			{
				GPIO_SetBits(GPIOA,GPIO_Pin_8);
				for(i=0;i<USART_SendLength;i++)
				{
					printf("%c\r\n",USART1_BUFF[i]);
				}
			}
			if(USART_SendLength==3&&USART1_BUFF[0]=='o'&&USART1_BUFF[1]=='f'&&USART1_BUFF[2]=='f')
			{
				GPIO_ResetBits(GPIOA,GPIO_Pin_8);
				for(i=0;i<USART_SendLength;i++)
				{
					printf("%c\r\n",USART1_BUFF[i]);
				}
			}
			USART1_RX_STA=0;
		}
	}
}


