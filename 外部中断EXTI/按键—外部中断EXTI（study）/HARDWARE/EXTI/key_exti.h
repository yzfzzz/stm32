#ifndef __KEY_EXTI__
#define __KEY_EXTI__

#include "sys.h"
#include "led.h"

#define KEY_EXTI_GPIO_CLK   RCC_APB2Periph_GPIOA
#define KEY_EXTI_GPIO_PORT  GPIOA
#define KEY_EXTI_GPIO_PIN   GPIO_Pin_0

void KEY_Init(void);
void KEY_GPIO_Init(void);
void KEY_EXTI_Init(void);
void KEY_NVIC_Init(void);

#endif
