#ifndef __KEY_EXTI__
#define __KEY_EXTI__

#define STM32RCT6 1 //嵌入式精品网
#define STM32C8T6 0

#include "sys.h"
#include "led.h"

#if STM32RCT6
#define KEY1_EXTI_GPIO_CLK          RCC_APB2Periph_GPIOA
#define KEY1_EXTI_GPIO_PORT         GPIOA
#define KEY1_EXTI_GPIO_PIN          GPIO_Pin_0

#define KEY1_EXTI_GPIO_PORTSOURCE   GPIO_PortSourceGPIOA
#define KEY1_EXTI_GPIO_PINSOURCE    GPIO_PinSource0
#define KEY1_EXTI_Line              EXTI_Line0
#define KEY1_EXTI_IRQN              EXTI0_IRQn
#define KEY1_EXTI_IRQHANDLER        EXTI0_IRQHandler

#elif STM32C8T6

#endif

void KEY_Init(void);
void KEY_GPIO_Init(void);
void KEY_EXTI_Init(void);
void KEY_NVIC_Init(void);

#endif
