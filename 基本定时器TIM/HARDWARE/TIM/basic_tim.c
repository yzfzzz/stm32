#include "basic_tim.h"


/**************************************************************************
函数功能：对基本定时器6，7中断服务函数
入口参数：N是测试中断的参数，开发时可不用
引	 脚：无
**************************************************************************/
int N;
void TIM6_IRQHandler()
{
    if(TIM_GetFlagStatus(TIM6,TIM_FLAG_Update)!=RESET)
    {
        N++;
    }
   TIM_ClearITPendingBit(TIM6,TIM_IT_Update);
}

void TIM7_IRQHandler()
{
    if(TIM_GetFlagStatus(TIM7,TIM_FLAG_Update)!=RESET)
    {
        N++;
    }
   TIM_ClearITPendingBit(TIM7,TIM_IT_Update);
}



/**************************************************************************
函数功能：对基本定时器总初始化
入口参数：无
引	 脚：无
**************************************************************************/
void BasicTIM_Init()
{
    BasicTIM_NVIC_Init();
    BasicTIM_Mode_Init();
}

/**************************************************************************
函数功能：对基本定时器中断初始化
入口参数：无
引	 脚：无
**************************************************************************/
void BasicTIM_NVIC_Init()
{
     
    NVIC_InitTypeDef NVIC_InitStructure;
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    NVIC_InitStructure.NVIC_IRQChannel=TIM_IRQN;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=NVIC_HIGHT_PRIORITY;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority=NVIC_LOW_PRIORITY;
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;

    NVIC_Init(&NVIC_InitStructure);
}

/**************************************************************************
函数功能：对基本定时器GPIO初始化
入口参数：无
引	 脚：无
**************************************************************************/
void BasicTIM_Mode_Init()
{
    TIM_TimeBaseInitTypeDef TIM_BaseInitStructure;
    RCC_APB1PeriphClockCmd(TIM_CLK,ENABLE);

    TIM_BaseInitStructure.TIM_Prescaler=TIM_PRE-1;
    TIM_BaseInitStructure.TIM_Period=TIM_ARR-1;

    TIM_TimeBaseInit(BASICTIM,&TIM_BaseInitStructure);

    TIM_ClearFlag(BASICTIM,TIM_FLAG_Update);//清除更新中断标志位

    TIM_ITConfig(BASICTIM,TIM_IT_Update,ENABLE);//TIM_IT_Update，开启更新中断

    TIM_Cmd(BASICTIM,ENABLE);//打开定时器
}




