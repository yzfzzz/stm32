#ifndef __LED_H__
#define __LED_H__
#include "sys.h"

//对STM32RCT6 IO口进行配置
#define STM32RCT6   1   //嵌入式精品网mini，只有一个LED
#define STM32C8T6   0  //洋桃电子C8T6

#if STM32RCT6 
#define LED1_GPIO_CLK        RCC_APB2Periph_GPIOA
#define LED1_GPIO_PORT       GPIOA
#define LED1_GPIO_PIN        GPIO_Pin_8


#elif STM32C8T6
#define LED1_GPIO_CLK        RCC_APB2Periph_GPIOB
#define LED1_GPIO_PORT       GPIOB
#define LED1_GPIO_PIN        GPIO_Pin_0

#define LED2_GPIO_CLK        RCC_APB2Periph_GPIOB
#define LED2_GPIO_PORT       GPIOB
#define LED2_GPIO_PIN        GPIO_Pin_1

#endif

void LED_Init(void);
void LED1_Control(unsigned int time,u8 flash,u8 onoff);


#endif
