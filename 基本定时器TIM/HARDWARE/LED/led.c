#include "led.h"

void LED_Init()
{
    GPIO_InitTypeDef GPIOStructure;

    RCC_APB2PeriphClockCmd(LED1_GPIO_CLK, ENABLE);
    //RCC_APB2PeriphClockCmd(LED2_GPIO_CLK, ENABLE);

    GPIOStructure.GPIO_Pin = LED1_GPIO_PIN;
    GPIOStructure.GPIO_Mode =  GPIO_Mode_Out_PP;
    GPIOStructure.GPIO_Speed =GPIO_Speed_10MHz;

    //GPIOStructure.GPIO_Pin = LED2_GPIO_PIN;
    //GPIOStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    //GPIOStructure.GPIO_Speed =GPIO_Speed_10MHz;

    GPIO_Init(LED1_GPIO_PORT , &GPIOStructure);
    //GPIO_Init(LED2_GPIO_PORT , &GPIOStructure);
}

/**************************************************************************
函数功能：LED1的控制
入口参数：1.闪烁频率（即delay时间）   2.是否闪烁（是1、否0） 3.非闪烁状态下灯的亮灭（亮1、灭0）
引	 脚：LED1
**************************************************************************/
void LED1_Control(unsigned int time,u8 flash,u8 onoff)
{
        if(flash)
        {
            GPIO_SetBits(LED1_GPIO_PORT,LED1_GPIO_PIN);
		    delay_ms(time);
		    GPIO_ResetBits(LED1_GPIO_PORT,LED1_GPIO_PIN);
		    delay_ms(time);
        }
        else
        {
            if(onoff)
            {
                GPIO_ResetBits(LED1_GPIO_PORT,LED1_GPIO_PIN);
            }
            else
            {
                GPIO_SetBits(LED1_GPIO_PORT,LED1_GPIO_PIN);
            }
        }

}

