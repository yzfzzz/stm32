#include "advanced_tim.h"

/**************************************************************************
函数名：AdvancedTIM_Init
函数功能：高级定时器总的初始化
入口参数：无
引	 脚：无
**************************************************************************/
void AdvancedTIM_Init()
{
  AdvancedTIM_GPIO_Init();
  AdvancedTIM_Base_Init();
  AdvancedTIM_OC_Init();
  AdvancedTIM_BDTR_Init();
}


/**************************************************************************
函数名：AdvancedTIM_GPIO_Init()
函数功能：高级定时器GPIO的初始化
入口参数：无
引	 脚:PA6(TIM8_BKIN)，PA7(TIM8_CH1N),PC6(TIM8_CH1)
        PB12(TIM1_BKIN)，PA8(TIM8_CH1),PB13(TIM1_CH1)
**************************************************************************/
static void AdvancedTIM_GPIO_Init()
{
  GPIO_InitTypeDef GPIO_InitStructure;

  //输出比较通道GPIO初始化
  RCC_APB2PeriphClockCmd(Advanced_TIM_CH1_GPIO_CLK, ENABLE);
  GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin=Advanced_TIM_CH1_PIN;
  GPIO_Init(Advanced_TIM_CH1_PORT,&GPIO_InitStructure);

  //输出比较互补通道GPIO初始化
  RCC_APB2PeriphClockCmd(Advanced_TIM_CHN1_GPIO_CLK, ENABLE);
  GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin=Advanced_TIM_CHN1_PIN;
  GPIO_Init(Advanced_TIM_CHN1_PORT,&GPIO_InitStructure);

  //输出比较通道的刹车通道
  RCC_APB2PeriphClockCmd(Advanced_TIM_BKIN_GPIO_CLK, ENABLE);
  GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin=Advanced_TIM_BKIN_PIN;
  GPIO_Init(Advanced_TIM_BKIN_PORT,&GPIO_InitStructure);
}



/**************************************************************************
函数名：AdvancedTIM_Base_Init()
函数功能：高级定时器时基的初始化
入口参数：无
引	 脚：无
**************************************************************************/
static void AdvancedTIM_Base_Init()
{
    TIM_TimeBaseInitTypeDef TIM_BaseInitStructure;
    RCC_APB2PeriphClockCmd(Advanced_TIM_CLK, ENABLE);

    TIM_BaseInitStructure.TIM_Prescaler = Advanced_TIM_PSC;
    TIM_BaseInitStructure.TIM_Period = Advanced_TIM_PERIOD;
    TIM_BaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;//设置时钟分频因子
    TIM_BaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;//设置为向上计数
    TIM_BaseInitStructure.TIM_RepetitionCounter=0;//设置重复计数器的值，这里不需要重复计数，所以为0

    TIM_TimeBaseInit(Advanced_TIM,&TIM_BaseInitStructure);
}



/**************************************************************************
函数名：AdvancedTIM_OC_Init()
函数功能：高级定时器输出比较结构体的初始化
入口参数：无
引	 脚：无
**************************************************************************/
static void AdvancedTIM_OC_Init()
{
  TIM_OCInitTypeDef TIM_OCInitStructure;

  TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;//配置为PWM1模式
  TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;//允许输出通过IO与外界相连
  TIM_OCInitStructure.TIM_OutputNState=TIM_OutputNState_Enable;//允许互补输出通过IO与外界相连
  TIM_OCInitStructure.TIM_Pulse=Advanced_TIM_PULSE;//设置占空比
  TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;//设置定时器输出的是高电平还是低电平
  TIM_OCInitStructure.TIM_OCNPolarity=TIM_OCNPolarity_High;
  TIM_OCInitStructure.TIM_OCIdleState=TIM_OCIdleState_Set;//设置在空闲状态下，定时器输出什么电平
  TIM_OCInitStructure.TIM_OCNIdleState=TIM_OCNIdleState_Reset;

  TIM_OC1Init(Advanced_TIM,&TIM_OCInitStructure);
  TIM_OC1PreloadConfig(Advanced_TIM,TIM_OCPreload_Enable);

  TIM_Cmd(Advanced_TIM, ENABLE);

  //主输出使能，当使用的是通用定时器，这句不需要
  TIM_CtrlPWMOutputs(Advanced_TIM, ENABLE);
}


/**************************************************************************
函数名：AdvancedTIM_BDTR_Init()
函数功能：高级定时器刹车和死区结构体的初始化
入口参数：无
引	 脚：无
**************************************************************************/
static void AdvancedTIM_BDTR_Init()
{
  TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
  
  TIM_BDTRInitStructure.TIM_OSSIState=TIM_OSSIState_Enable;//空闲模式下的关闭状态选择
  TIM_BDTRInitStructure.TIM_OSSRState=TIM_OSSRState_Enable;//运行模式下的关闭状态选择
  TIM_BDTRInitStructure.TIM_LOCKLevel=TIM_LOCKLevel_1;//锁存配置

  TIM_BDTRInitStructure.TIM_DeadTime=11;//死区时间
  TIM_BDTRInitStructure.TIM_Break=TIM_Break_Enable;//断路输入使能控制

  TIM_BDTRInitStructure.TIM_BreakPolarity=TIM_BreakPolarity_High;//断路输入极性
  TIM_BDTRInitStructure.TIM_AutomaticOutput=TIM_AutomaticOutput_Enable;//自动输出极性
  TIM_BDTRConfig(Advanced_TIM,&TIM_BDTRInitStructure);
}
