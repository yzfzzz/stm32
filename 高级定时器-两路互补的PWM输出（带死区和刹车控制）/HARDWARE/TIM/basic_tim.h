#ifndef __BASIC_TIM_H__
#define __BASIC_TIM_H__
#include "sys.h"

//本代码仅可对一个基本定时器初始化！
#define Basic_TIM6    0//定义用哪一个基本定时器
#define Basic_TIM7    1

#if Basic_TIM6 
#define BASICTIM                TIM6 //定义定时器
#define TIM_IRQN                TIM6_IRQn//定义中断源
#define NVIC_HIGHT_PRIORITY     1//主优先级（0~3）
#define NVIC_LOW_PRIORITY       3//从优先级（0~3）
#define TIM_CLK                 RCC_APB1Periph_TIM6//定时器6时钟，不用改
#define TIM_PRE                 72
#define TIM_ARR                 1000 

//定时器时间的计算：（PRE*ARR）/(72*10^6);单位是 秒（s）

#elif Basic_TIM7 
#define BASICTIM                TIM7 //定义定时器
#define TIM_IRQN                TIM7_IRQn//定义中断源
#define NVIC_HIGHT_PRIORITY     1//主优先级（0~3）
#define NVIC_LOW_PRIORITY       3//从优先级（0~3）
#define TIM_CLK                 RCC_APB1Periph_TIM7//定时器7时钟，不用改
#define TIM_PRE                 72
#define TIM_ARR                 1000 
#endif


void BasicTIM_Mode_Init(void);
void BasicTIM_NVIC_Init(void);
void BasicTIM_Init(void);
extern int N;

#endif
