#ifndef __UART__
#define __UART__

#include "sys.h"
#include "stdio.h"


void UART1_Init(int bound);
void UART2_Init(int bound);
void UART3_Init(int bound);
void UART_SendByte(USART_TypeDef *p_USARTx, uint8_t ch);
void UART_SendString(USART_TypeDef *p_USARTx,char *str);

#endif /* __UART__ */
