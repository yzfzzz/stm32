#include "led.h"

void LED_Init()
{
	GPIO_InitTypeDef GPIO_InitStruct;
//第一步：打开外部时钟 
/**作用：开启APB2外部时钟使能
	*1st参数：外部时钟的寄存器
	*@arg RCC_APB2Periph_AFIO, RCC_APB2Periph_GPIOA, RCC_APB2Periph_GPIOB,
  *          RCC_APB2Periph_GPIOC, RCC_APB2Periph_GPIOD, RCC_APB2Periph_GPIOE,
  *          RCC_APB2Periph_GPIOF, RCC_APB2Periph_GPIOG, RCC_APB2Periph_ADC1,
  *          RCC_APB2Periph_ADC2, RCC_APB2Periph_TIM1, RCC_APB2Periph_SPI1,
  *          RCC_APB2Periph_TIM8, RCC_APB2Periph_USART1, RCC_APB2Periph_ADC3,
  *          RCC_APB2Periph_TIM15, RCC_APB2Periph_TIM16, RCC_APB2Periph_TIM17,
  *          RCC_APB2Periph_TIM9, RCC_APB2Periph_TIM10, RCC_APB2Periph_TIM11
	*2st：ENABLE or DISABLE （是否使能）
  */
  //void RCC_APB2PeriphClockCmd(uint32_t RCC_APB2Periph, FunctionalState NewState)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
//第二步：配置初始化结构体
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;//设置推挽输出
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_10MHz;//速度越快，功耗越高
	
//第三步：调用外设初始化函数，把配置好的结构体成员写入相应的寄存器中
//void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_InitStruct)
/**作用：将结构体成员写入寄存器当中
	*1st参数：GPIO端口，即GPIOx （x=A ...G)
	*2st参数：结构体的指针
  * @brief  Initializes the GPIOx peripheral according to the specified
  *         parameters in the GPIO_InitStruct.
  * @param  GPIOx: where x can be (A..G) to select the GPIO peripheral.
  * @param  GPIO_InitStruct: pointer to a GPIO_InitTypeDef structure that
  *         contains the configuration information for the specified GPIO peripheral.
  * @retval None
  */
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
}




